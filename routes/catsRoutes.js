const express = require("express");
const router = express.Router();

const {
  getAllCat,
  createCat,
  getCat,
  updateCat,
  deleteCat,
  searchCat,
} = require("../controllers/catsController");

router.route("/").get(getAllCat).post(createCat);
router.route("/search").get(searchCat);
router.route("/:id").get(getCat).put(updateCat).delete(deleteCat);

module.exports = router;

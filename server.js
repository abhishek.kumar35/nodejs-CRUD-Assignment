const express = require("express");
const cats = require("./routes/catsRoutes");
require("dotenv").config();
const connectDB = require("./config/db");
const PORT = 5000;

connectDB();

const app = express();

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// routes
app.get('/',(req,res)=>{
  res.send("hello world")
})

app.use("/api/cats", cats);


app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

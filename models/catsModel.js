const mongoose = require("mongoose");

const CatSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "Must Provide Name"],
  },
  age: {
    type: Number,
    required: [true, "Must Provide Age"],
  },
  breed: {
    type: String,
    required: [true, "Must Provide Breed"],
  },
});

module.exports = mongoose.model("Cat", CatSchema);

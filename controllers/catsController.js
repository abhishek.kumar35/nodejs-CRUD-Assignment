const Cat = require("../models/catsModel");
const asyncHandler = require("express-async-handler");


// @desc Get all Cats
// @route GET /api/cats/
// @access Public
const getAllCat = asyncHandler(async (req,res) =>{
  const allCats = await Cat.find();

  res.status(200).json(allCats);
});

// @desc Create new cat
// @route POST /api/cats
// @access Public
const createCat = asyncHandler(async (req,res)=>{
  const {name, age, breed}=req.body

  if (!name || !age || !breed) {
    res.status(400);
    throw new Error("Please add all feilds");
  }
  // Create Cat
  const cat = await Cat.create({
    name,
    age,
    breed
  });
  if(cat){
    res.status(201).json({
      _id:cat.id,
      name:cat.name,
      age:cat.age,
      breed:cat.breed
    });
  } else {
    res.status(400);
    throw new Error("Invalid Cat Data")
  }
});
// @desc Get cat by id
// @route GET /api/cats/:id
// @access Public

const getCat = asyncHandler(async (req,res)=>{
  const cat = await Cat.findById(req.params.id );

  if(!cat){
    res.status(400);
    throw new Error("Cat not found")
  } else {
    res.status(200).json(cat);
  }
})

// @desc Update Cat Data
// @route PUT /api/cats/:id
// @access Public
const updateCat = asyncHandler(async (req,res)=>{
   const cat = await Cat.findById(req.params.id);

   if(!cat){
     res.status(400);
     throw new Error("Cat not found");
   }

   const updatedCat = await Cat.findByIdAndUpdate(req.params.id, req.body, {
     new:true,
   });
   res.status(200).json(updateCat)
})

// @desc Delete cat
// @route DELETE /api/cats/:id
// @access Public
const deleteCat = asyncHandler(async (req,res)=>{
  const cat = await Cat.findById(req.params.id);

  if(!cat){
    res.status(400);
    throw new Error("Cat not found");
  }

  await cat.remove()
  res.status(200).json({id:req.params.id})
})

// @desc Search cat
// @route GET /api/cats/search
// @access Public
const searchCat = asyncHandler(async (req,res)=>{
  if(req.query.age_lte>req.query.age_gte){
    res.status(400);
    throw new Error("Minimum age cannot be greater than the maximum age!")
  }
  const cat = await Cat.find({
    age: { $gte: req.query.age_lte, $lte: req.query.age_gte },
  });

  console.log(cat);
  if(cat.length===0){
    res.status(400);
    throw new Error("No cats are available in the given age range!")
  } else {
    res.status(200).json(cat);
  }
  
})

module.exports = {
  getAllCat,
  createCat,
  getCat,
  updateCat,
  deleteCat,
  searchCat,
};
